//
//  CLLocationCoordinate2D+Extension.swift
//  spotfinder
//
//  Created by Moy Hdez on 22/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationCoordinate2D {
    // In meteres
    func readableDistance(to: CLLocationCoordinate2D) -> String {
        let from = CLLocation(latitude: self.latitude, longitude: self.longitude)
        let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
        var distance = from.distance(from: to)
        var format: String!
        if (distance < 100) {
            format = "%@ m";
            distance = Double(Int(distance))
        } else {
            format = "%@ km";
            distance = distance / 1000
        }
        return String(format: format, string(with: distance))
    }
    
    func string(with value: Double) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = NSLocale.current
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 1
        return numberFormatter.string(from: value as NSNumber)!
    }
    
}
