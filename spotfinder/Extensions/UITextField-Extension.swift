//
//  UITextField+-Extension.swift
//  spotfinder
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

extension String {
    
    func removeCommas() -> String {
        return self.replacingOccurrences(of: ",", with: "")
    }
    
    func toInt() -> Int {
        return (self as NSString).integerValue
    }
    
    func toNSNumber() -> NSNumber {
        return NSNumber(value: self.toInt())
    }
    
    func toFloat() -> Float {
        return (self as NSString).floatValue
    }
    
    func toDouble() -> Double {
        return (self as NSString).doubleValue
    }
}

extension Int {
    func toString() -> String {
        return String(self)
    }
}

extension Float {
    var removeDecimals: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

extension NSObject {
    var theClassName: String {
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
}
extension UINavigationController {
    func pushViewController(viewController: UIViewController, animated: Bool, completion: @escaping () -> ()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.pushViewController(viewController, animated: true)
        CATransaction.commit()
    }
    
    func popViewController(animated: Bool, completion: @escaping () -> ()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.popViewController(animated: true)
        CATransaction.commit()
    }
}

extension UILabel {
    func underLineText(text: String){
        let string = self.text!
        let range = (string as NSString).range(of: text)
        let attrString = NSMutableAttributedString(string: string)
        
        attrString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSNumber(value: 1), range: range)
        
        self.attributedText = attrString
    }
}
