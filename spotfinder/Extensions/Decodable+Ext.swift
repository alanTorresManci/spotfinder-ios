//
//  Decodable+Ext.swift
//  VinosBajaCalifornia
//
//  Created by Moy Hdez on 14/11/17.
//  Copyright © 2017 tejuino. All rights reserved.
//

import Foundation

extension Decodable {
    
    init(_ any: JSON) throws {
        let data = try JSONSerialization.data(withJSONObject: any, options: .prettyPrinted)
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:sszzz"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        self = try decoder.decode(Self.self, from: data)
    }
}
