//
//  UIImageView-Extension.swift
//  spotfinder
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import AlamofireImage

extension UIImageView {
    
    func setTint(color: UIColor) {
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
    
    func downloadWithLoader(url: URL?, placeHolder: UIImage? = nil, color: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), largeLoader: Bool = false, completion: ((_ image: UIImage?, _ success: Bool) -> ())? = nil) {
        self.image = nil
        
        guard let url = url else {
            return
        }
        
        let indicator = UIActivityIndicatorView()
        
        if largeLoader {
            indicator.activityIndicatorViewStyle = .whiteLarge
        }
        
        indicator.color = color
        
        self.addSubview(indicator)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint(item: indicator, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: indicator, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        indicator.startAnimating()
        
        self.af_setImage(withURL: url, imageTransition: .crossDissolve(0.2), completion: {response in
            if response.result.isSuccess {
                if completion != nil {
                    completion!(response.result.value, true)
                }
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }else {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
                if completion != nil {
                    completion!(nil, false)
                }
                
                if placeHolder != nil {
                    self.image = placeHolder
                }
            }
        })
    }
    
}
