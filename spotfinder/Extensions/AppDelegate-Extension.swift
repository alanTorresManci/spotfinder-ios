//
//  AppDelegate-Extension.swift
//  spotfinder
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import Firebase
import UserNotifications
//import GLNotificationBar

extension AppDelegate {
    
    // AppDelegate SharedInstance
    static func sharedInstance() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    static func checkUser(_ delegate: UIViewController) -> Bool {
        if User.getFromDefaults() == nil {
            let vc = LoginNC.instance()
            delegate.present(vc, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func switchRootViewController(vc: UIViewController, animated: Bool, completion: (()-> Void)? = nil){
        if animated{
            UIView.transition(with: self.window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                let oldState: Bool = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                self.window?.rootViewController = vc
                UIView.setAnimationsEnabled(oldState)
            }, completion: { (finished: Bool) in
                if completion != nil{
                    completion!()
                }
            })
        }else{
            self.window?.rootViewController = vc
        }
    }
    
    func topMostController() -> UIViewController {
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    static func logout() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: K.Defaults.AccessToken)
        defaults.removeObject(forKey: K.Defaults.RefreshToken)
        defaults.removeObject(forKey: K.Defaults.User)
        AppDelegate.sharedInstance().switchRootViewController(vc: MainTabBarController.instance(), animated: true)
    }
    
    func open(url: URL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    static func openMap(_ delegate: UIViewController, with coordinates: CLLocationCoordinate2D) {
        let menuView = UIAlertController(title: "Open with", message: "", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            menuView.dismiss(animated: true, completion: nil)
        }
        
        let googleMapsUrl = URL(fileURLWithPath: "comgooglemaps://")
        let wazeUrl = URL(fileURLWithPath: "waze://")
        let appleMapsUrl = URL(fileURLWithPath: "http://maps.apple.com")
        
        let customGoogleMapsUrl = "comgooglemaps://?saddr=\(coordinates.latitude),\(coordinates.longitude)" //&directionsmode=driving
        let customWazeUrl = "waze://?ll=\(coordinates.latitude),\(coordinates.longitude)" //&navigate=yes
        let customAppleMapsUrl = "http://maps.apple.com/?daddr=\(coordinates.latitude),\(coordinates.longitude)"//&dirflg=d&t=m
        // eval if the app google maps is installed
        if UIApplication.shared.canOpenURL(googleMapsUrl) {
            let action = UIAlertAction(title: "Google Maps", style: .default) { (action) in
                if let url = URL(string: customGoogleMapsUrl) {
                    AppDelegate.sharedInstance().open(url: url)
                }
            }
            menuView.addAction(action)
        }
        // eval if waze is installed
        if UIApplication.shared.canOpenURL(wazeUrl) {
            let action = UIAlertAction(title: "Waze", style: .default) { (action) in
                if let url = URL(string: customWazeUrl) {
                    AppDelegate.sharedInstance().open(url: url)
                }
            }
            menuView.addAction(action)
        }
        // eval if apple maps is installed
        if UIApplication.shared.canOpenURL(appleMapsUrl) {
            let action = UIAlertAction(title: "Mapas", style: .default) { (action) in
                if let url = URL(string: customAppleMapsUrl) {
                    AppDelegate.sharedInstance().open(url: url)
                }
            }
            menuView.addAction(action)
        }
        
        menuView.addAction(cancelAction)
        delegate.present(menuView, animated: true)
    }
    
}

//MARK: PUSH NOTIFICATION
extension AppDelegate {
    
    static func getDeviceId() -> Int? {
        let deviceID = UserDefaults.standard.integer(forKey: "device_id")
        return deviceID > 0 ? deviceID : nil
    }
    
    static func setDeviceId(deviceId: Int) {
        UserDefaults.standard.set(deviceId, forKey: "device_id")
    }
    
    func registerForPushNotifications() {
        let application = UIApplication.shared
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func firebaseConfigure() {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func createCustomLocalNotification(userInfo: [AnyHashable:Any]) {
        
//        guard let aps = userInfo["aps"] as? [String:AnyObject] else { return }
//        guard let title = aps["alert"]?["title"] as? String else { return }
//        guard let body = aps["alert"]?["body"] as? String else { return }
//
//        let notificationBar = GLNotificationBar(title: title, message: body, preferredStyle: .simpleBanner) { (areTaped) in
//            if areTaped {
//                AppDependencies.sharedInstance.notificationHandler(userInfo: userInfo)
//            }
//        }
//
//        notificationBar.notificationSound("", ofType: "", vibrate: true)
        
    }
    
    func incrementBadgeNumberBy(badgeNumberIncrement: Int) {
        let currentBadgeNumber = UIApplication.shared.applicationIconBadgeNumber
        let updatedBadgeNumber = currentBadgeNumber + badgeNumberIncrement
        if (updatedBadgeNumber > -1) {
            UIApplication.shared.applicationIconBadgeNumber = updatedBadgeNumber
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Fail To register remote notification: \(error)")
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("didReceiveRemoteNotification: \(userInfo)")
        if application.applicationState == .inactive || application.applicationState == .background {
            
        } else {

        }
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().setAPNSToken(deviceToken, type: .unknown)

        if let token = Messaging.messaging().fcmToken {
            print("TOKEN: \(token)")
            registerDevice(token: token)
        }
    }
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("TOKEN REFRESH: \(fcmToken)")
        registerDevice(token: fcmToken)
    }
    
    func registerDevice(token: String) {
        if User.getFromDefaults() == nil {
            return
        }
        var parameters: JSON = ["os": "ios",
                                "token": token]
        if let deviceID = AppDelegate.getDeviceId() {
            parameters.updateValue("\(deviceID)", forKey: "device_id")
            print("old device_id(UPDATE FCM): \(deviceID)")
        }
        
        
        APIClient().request(request: .UpdateFCMToken(params: parameters)) { (response, success) in
            if success {
                guard let deviceId = response.data["device_id"] as? Int else { return }
                print("new device_id(UPDATE FCM): \(deviceId)")
                AppDelegate.setDeviceId(deviceId: deviceId)
            }
        }
        
    }

}

