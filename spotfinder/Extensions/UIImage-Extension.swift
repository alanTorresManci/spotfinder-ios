//
//  UIImage-Extension.swift
//  spotfinder
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

extension UIImage {
    
    static func download(url: URL, completion: @escaping (_ image:UIImage?) -> Void) {
        Alamofire.request(url).responseImage { response in
            completion(response.result.value)
        }
    }
    
    func resizeImage(maxSize: CGFloat) -> UIImage! {
        let imageSize = self.size
        if imageSize.width <= maxSize && imageSize.height <= maxSize { return self }
        
        var scaledSize = CGSize(width: 0, height: 0)
        
        if imageSize.width > imageSize.height {
            scaledSize.width = maxSize
            scaledSize.height = (imageSize.height * maxSize) / imageSize.width;
        }else {
            scaledSize.height = maxSize
            scaledSize.width = (imageSize.width * maxSize) / imageSize.height
        }
        
        let rect = CGRect(x: 0, y: 0, width: scaledSize.width, height: scaledSize.height)
        UIGraphicsBeginImageContextWithOptions(scaledSize, false, 1)
        self.draw(in: rect)
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage
    }
    
    func resizeImageRect(width: CGFloat, ratio: CGFloat) -> UIImage! {
        let scaledSize = CGSize(width: width, height: width * ratio)
        
        let rect = CGRect(x: 0, y: 0, width: scaledSize.width, height: scaledSize.height)
        UIGraphicsBeginImageContextWithOptions(scaledSize, false, 1)
        self.draw(in: rect)
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    func base64() -> String {
        let imageData = UIImageJPEGRepresentation(self, 0.8)
        let base64 = imageData!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        let imageBase64 = "data:image/png;base64,\(base64)"
        return imageBase64
    }
}
