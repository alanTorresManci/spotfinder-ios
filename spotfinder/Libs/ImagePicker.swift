//
//  ImagePicker.swift
//  spotfinder
//
//  Created by Moy Hdez on 22/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class ImagePicker: NSObject {
    
    // MARK: Localized strings
    
    static let actionTitleCancel: String = "Cancel"
    
    static let actionTitleTakePhoto: String = "Take photo"
    
    static let actionTitleLibrary: String = "Select photo"
    
    // MARK: Error's definition
    enum ErrorImagePicker: String {
        case CameraNotAvailable = "Camera not available"
        case LibraryNotAvailable = "Library not available"
        case AccessDeniedCameraRoll = "Access denied to camera roll"
        case EntitlementiCloud = "Missing iCloud Capatability"
        case WrongFileType = "Wrong file type"
        case PopoverTarget = "Missing property popoverTarget while iPad is run"
        case Other = "Other"
    }
    
    static func pickPhoto<T: UIViewController>(vc: T) where T: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = vc
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: ImagePicker.actionTitleTakePhoto, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("File Deleted")
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.allowsEditing = false
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.cameraCaptureMode = .photo
                imagePicker.modalPresentationStyle = .fullScreen
                imagePicker.mediaTypes = ["public.image"]
                vc.present(imagePicker,animated: true,completion: nil)
            } else {
                AlertsController.showAlert(delegate: vc, title: "", message: ErrorImagePicker.CameraNotAvailable.rawValue, OKButtonTitle: "OK")
            }
            
        })
        
        let saveAction = UIAlertAction(title: ImagePicker.actionTitleLibrary, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("File Saved")
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.mediaTypes = ["public.image"]
            vc.present(imagePicker, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: ImagePicker.actionTitleCancel, style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        vc.present(optionMenu, animated: true, completion: nil)
    }
    
}
