//
//  PickerVC.swift
//  Kubaz
//
//  Created by Moy Hdez on 08/03/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
class PickerVC: UIViewController {
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var picker: UIPickerView!
    
    var completion: ((Int,Int)->Void)?
    
    var onExit: (()->Void)?
    
    var updateLbl: Bool = false
    
    var lbl: UILabel?
    
    var list: [[String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        self.backgroundView.alpha = 0
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            self.backgroundView.alpha = 0.7
        }, completion: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.backgroundView.alpha = 0
        }, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        picker.reloadAllComponents()
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
    func exit(){
        self.dismiss(animated: false, completion: nil)
        onExit?()
    }
    
    func setSelected(component: Int){
        for listA in list{
            if listA.isEmpty{
                return
            }
        }
        
        let row = picker.selectedRow(inComponent: component)
        print("ROW: \(row) Component: \(component)")
        if updateLbl{
            lbl?.text = list[component][row]
        }
        completion?(component,row)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        for i in 0..<list.count{
            setSelected(component: i)
        }
        exit()
    }
    
    @IBAction func exitAction(_ sender: Any) {
        for i in 0..<list.count{
            setSelected(component: i)
        }
        exit()
    }
}

extension PickerVC: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return list[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row:
        Int, inComponent component: Int) {
        setSelected(component: component)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return list[component][row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return list.count
    }
    
}
