//
//  ProfileVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 24/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation


class ProfileVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var refreshControl: UIRefreshControl!
    
    var locationManager = CLLocationManager()
    var userLocation: CLLocationCoordinate2D?
    var locationSentFavs: CLLocationCoordinate2D?
    var locationSentMine: CLLocationCoordinate2D?
    
    var paginationFavs: Pagination?
    var isLoadingFavs = false
    var paginationMine: Pagination?
    var isLoadingMine = false
    
    var spots = [Spot]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var mySpots = [Spot]()
    var myFavorites = [Spot]()

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let _ = User.getFromDefaults() else {
            self.navigationController?.tabBarController?.selectedIndex = 0
            return
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "settings_unfilled"), style: .plain, target: self, action: #selector(settings))
        setUpTableView()
        setUpLocation()
        getMySpots()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.relayoutTableHeaderView()
        profileImg.setCornerRadius(4)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fillData()
    }
    
    func setUpTableView() {
        tableView.estimatedRowHeight = 500
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(SpotCell.self)
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(self.refresh(sender:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    func setUpLocation() {
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func fillData() {
        let user = User.getFromDefaults()
        if let image = user?.image {
            profileImg.downloadWithLoader(url: image, color: UIColor.lightGray, largeLoader: false)
        }
        nameLbl.text = user?.name
    }
    
    @objc func refresh(sender:AnyObject) {
        refreshFromSelectedIndex(clear: true)
    }
    
    func refreshFromSelectedIndex(clear: Bool = false) {
        let selectecIndex = segmentedControl.selectedSegmentIndex
        switch selectecIndex {
        case 0:
            if clear {
                mySpots = []
                paginationMine = nil
                locationSentMine = nil
                if !refreshControl.isRefreshing {
                    LilithProgressHUD.show(self.view)
                }
            }
            self.spots = mySpots
            if self.spots.isEmpty {
                getMySpots()
            }
            break
        case 1:
            if clear {
                myFavorites = []
                paginationFavs = nil
                locationSentFavs = nil
                if !refreshControl.isRefreshing {
                    LilithProgressHUD.show(self.view)
                }
            }
            self.spots = myFavorites
            if self.spots.isEmpty {
                getMyFavorites()
            }
            break
        default:
            return
        }
    }
    
    @objc func settings() {
        let vc = SettingsVC.instance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func segmentedChanged(_ sender: Any) {
        refreshFromSelectedIndex()
    }
}

// MARK: - TableView
extension ProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    @objc func topBtnPressed(_ sender: UIButton) {
        let row = sender.tag
        let spot = spots[row]
        
        if User.getFromDefaults()?.id == spot.user?.id {
            showOptionsMenu(for: row)
        } else {
            if !AppDelegate.checkUser(self) { return }
            let indexPath = IndexPath(row: row, section: 0)
            spots[row].favorite = !spots[row].favorite
            tableView.reloadRows(at: [indexPath], with: .none)
            addToFavorites(indexPath, btn: sender)
        }
    }
    
    func deleteSpot(row: Int) {
        guard let id = spots[row].id else { return }
        APIClient().request(request: .DeleteSpot(vc: self, id: id)) { [weak self] (response, success) in
            if success, let `self` = self {
                self.spots.remove(at: row)
            }
        }
    }
    
    func showOptionsMenu(for row: Int){
        AlertsController.showActionSheet(delegate: self, buttons: NSLocalizedString("Delete", comment: ""), handler: { (str) in
            switch str {
            case NSLocalizedString("Delete", comment: ""):
                AlertsController.showAlert(delegate: self, title: NSLocalizedString("Delete spot", comment: ""), message: NSLocalizedString("Are you sure that you want to delete this spot?", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""), otherButtonTitle: NSLocalizedString("Cancel", comment: ""), success: {
                    self.deleteSpot(row: row)
                }, cancel: nil)
                break
            default:
                break
            }
        })
    }
    
    @objc func distanceBtnPressed(_ sender: UIButton) {
        let row = sender.tag
        let spot = spots[row]
        if let coordinates = spot.coordinates() {
            AppDelegate.openMap(self, with: coordinates)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spots.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        
        let cell =  tableView.dequeueReusableCell(for: indexPath) as SpotCell
        
        cell.setUpWith(spot: spots[row], location: userLocation)
        
        cell.distanceBtn.tag = row
        cell.topBtn.tag = row
        cell.distanceBtn.addTarget(self, action: #selector(distanceBtnPressed(_:)), for: .touchUpInside)
        cell.topBtn.addTarget(self, action: #selector(topBtnPressed(_:)), for: .touchUpInside)
        let slideShowTGR = UITapGestureRecognizer(target: self, action: #selector(self.didTapSlideShow(_:)))
        cell.slideshow.tag = row
        cell.slideshow.addGestureRecognizer(slideShowTGR)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? SpotCell {
            let spot = spots[indexPath.row]
            if let location = userLocation {
                cell.distanceLbl.text = spot.coordinates()?.readableDistance(to: location)
            }
        }
        if segmentedControl.selectedSegmentIndex == 0 {
            guard let pagination = paginationMine else { return }
            if indexPath.row == mySpots.count - 1 && mySpots.count < pagination.total && !isLoadingMine {
                getMySpots()
            }
        } else {
            guard let pagination = paginationFavs else { return }
            if indexPath.row == myFavorites.count - 1 && myFavorites.count < pagination.total && !isLoadingFavs {
                getMyFavorites()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cellTapped(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func cellTapped(at index: IndexPath) {
        let vc = DetailSpotVC.instance()
        vc.spot = spots[index.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTapSlideShow(_ sender: UITapGestureRecognizer){
        guard let row = sender.view?.tag else { return }
        let index = IndexPath(row: row, section: 0)
        cellTapped(at: index)
    }
}

//MARK: - Location manager
extension ProfileVC: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            if let coordinates = userLocation {
                if coordinates.latitude != location.coordinate.latitude && coordinates.longitude != location.coordinate.longitude{
                    userLocation = location.coordinate
                }
            } else {
                userLocation = location.coordinate
            }
        }
    }
}

// MARK: - Requests
extension ProfileVC {
    
    func addToFavorites(_ indexPath: IndexPath, btn: UIButton) {
        guard let spotId = self.spots[indexPath.row].id else { return }
        
        APIClient().request(request: .AddToFavorites(vc: self, id: spotId)) { (response, success) in
            if let favorite = response.data["favorite"] as? Bool, !favorite {
                self.spots.remove(at: indexPath.row)
            }
        }
    }
    
    func getMySpots(){
        var page = 1
        
        if let pagination = paginationMine {
            page = pagination.currentPage + 1
        }
        
        var params: JSON = [:]
        
        if var location = userLocation {
            if let locationSent = locationSentMine {
                location = locationSent
            }
            params.updateValue(location.latitude, forKey: "lat")
            params.updateValue(location.longitude, forKey: "lng")
            
            locationSentMine = location
        }
        
        isLoadingMine = true
        refreshControl.refreshManually()
        APIClient().request(request: .UserSpots(params: params, page: page)) { [weak self] (response, success) in
            self?.isLoadingMine = false
            guard let `self` = self else { return }
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            if success {
                guard let spotsData = response.data["spots"] as? [JSON],
                var spots = try? spotsData.flatMap(Spot.init) else { return }
                
                if let location = self.userLocation {
                    spots.sort(by: {$0.distance(to: location) < $1.distance(to: location)})
                }
                
                self.mySpots = spots
                if self.segmentedControl.selectedSegmentIndex == 0 {
                    self.spots = self.mySpots
                }
            }
        }
    }
    
    func getMyFavorites() {
        var page = 1
        
        if let pagination = paginationFavs {
            page = pagination.currentPage + 1
        }
        
        var params: JSON = [:]
        
        if var location = userLocation {
            if let locationSent = locationSentFavs {
                location = locationSent
            }
            params.updateValue(location.latitude, forKey: "lat")
            params.updateValue(location.longitude, forKey: "lng")
            
            locationSentFavs = location
        }
        
        isLoadingFavs = true
        refreshControl.refreshManually()
        APIClient().request(request: .UserFavorites(params: params, page: page)) { [weak self] (response, success) in
            self?.isLoadingFavs = false
            guard let `self` = self else { return }
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            if success {
                guard let spotsData = response.data["spots"] as? [JSON],
                    var spots = try? spotsData.flatMap(Spot.init) else { return }
                
                if let location = self.userLocation {
                    spots.sort(by: {$0.distance(to: location) < $1.distance(to: location)})
                }
                
                self.myFavorites = spots
                if self.segmentedControl.selectedSegmentIndex == 1 {
                    self.spots = self.myFavorites
                }
            }
        }
    }
}

