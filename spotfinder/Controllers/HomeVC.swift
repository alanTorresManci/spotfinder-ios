//
//  HomeVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation
import Simplicity
import GoogleMaps

class HomeVC: UIViewController {
    
    //MAPS API KEY
    //AIzaSyDcIfZmF_LxB-yRuDgkIVnYFOc9iy0IuLc
    
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var locationManager = CLLocationManager()
    var userLocation: CLLocationCoordinate2D?
    var locationSent: CLLocationCoordinate2D?
    var isFirstTime: Bool = true
    
    var pagination: Pagination?
    var isLoading = false
    var isInMap = false
    
    var maxDistance: String?
    
    var spots = [Spot]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var mapSpots = [(Spot, GMSMarker?)]() {
        didSet {
            addMapMarkers()
        }
    }
    
    var params: JSON = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "filter"), style: .plain, target: self, action: #selector(filter))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "map"), style: .plain, target: self, action: #selector(spotsViews))
        
        self.mapView.delegate = self
        self.mapView.settings.myLocationButton = true
        self.mapView.isMyLocationEnabled = true
        
        tableView.estimatedRowHeight = 500
        tableView.register(SpotCell.self)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(self.refresh(sender:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
        setUpLocation()
    }
    
    func setUpLocation() {
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        } else {
            refreshControl.refreshManually()
            LilithProgressHUD.show(self.view)
            getSpots()
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        clearAndGetSpots()
    }
    
    func clearAndGetSpots() {
        pagination = nil
        locationSent = nil
        if !refreshControl.isRefreshing {
            LilithProgressHUD.show(self.view)
        }
        getSpots()
    }
    
    func clearAndGetMapSpots() {
        mapView.clear()
        mapSpots = []
        getMapSpots(mapView)
    }
    
    func addMapMarkers() {
        //TODO: IMPROVE
        for i in (0..<mapSpots.count) {
            if mapSpots[i].1 != nil { continue }
            let spot = mapSpots[i].0
            guard let location = spot.coordinates() else { continue }
            let marker = GMSMarker(position: location)
            marker.map = mapView
            marker.userData = spot
            mapSpots[i] = (spot, marker)
            guard let image = spot.category?.image else { return }
            UIImage.download(url: image, completion: { (image) in
                if let image = image {
                    marker.icon = image.resizeImage(maxSize: 50)
                    marker.map = self.mapView
                }
            })
        }
    }
    
    @objc func spotsViews() {
        isInMap = !isInMap
        UIView.animate(withDuration: 0.3) {
            self.tableView.isHidden = self.isInMap
            self.tableView.alpha = self.isInMap ? 0 : 1
            self.mapView.isHidden = !self.isInMap
            self.mapView.alpha = !self.isInMap ? 0 : 1
            self.navigationItem.leftBarButtonItem?.image = self.isInMap ? #imageLiteral(resourceName: "list") : #imageLiteral(resourceName: "map")
        }
    }
    
    @objc func filter() {
        let filterVC = FilterVC.instance()
        filterVC.homeVC = self
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
}

// MARK: - TableView
extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    
    @objc func topBtnPressed(_ sender: UIButton) {
        let row = sender.tag
        let spot = spots[row]
        
        if User.getFromDefaults()?.id == spot.user?.id {
            showOptionsMenu(for: row)
        } else {
            if !AppDelegate.checkUser(self) { return }
            let indexPath = IndexPath(row: row, section: 0)
            spots[row].favorite = !spots[row].favorite
            tableView.reloadRows(at: [indexPath], with: .none)
            addToFavorites(indexPath, btn: sender)
        }
    }
    
    func showOptionsMenu(for row: Int){
        AlertsController.showActionSheet(delegate: self, buttons: NSLocalizedString("Delete", comment: ""), handler: { (str) in
            switch str {
            case NSLocalizedString("Delete", comment: ""):
                AlertsController.showAlert(delegate: self, title: NSLocalizedString("Delete spot", comment: ""), message: NSLocalizedString("Are you sure that you want to delete this spot?", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""), otherButtonTitle: NSLocalizedString("Cancel", comment: ""), success: {
                    self.deleteSpot(row: row)
                }, cancel: nil)
                break
            default:
                break
            }
        })
    }
    
    @objc func distanceBtnPressed(_ sender: UIButton) {
        let row = sender.tag
        let spot = spots[row]
        if let coordinates = spot.coordinates() {
            AppDelegate.openMap(self, with: coordinates)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spots.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        
        let cell =  tableView.dequeueReusableCell(for: indexPath) as SpotCell
        
        cell.setUpWith(spot: spots[row], location: userLocation)
        
        cell.distanceBtn.tag = row
        cell.topBtn.tag = row
        cell.distanceBtn.addTarget(self, action: #selector(distanceBtnPressed(_:)), for: .touchUpInside)
        cell.topBtn.addTarget(self, action: #selector(topBtnPressed(_:)), for: .touchUpInside)
        let slideShowTGR = UITapGestureRecognizer(target: self, action: #selector(self.didTapSlideShow(_:)))
        cell.slideshow.tag = row
        cell.slideshow.addGestureRecognizer(slideShowTGR)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? SpotCell {
            let spot = spots[indexPath.row]
            if let location = userLocation {
                cell.distanceLbl.text = spot.coordinates()?.readableDistance(to: location)
            }
        }
        guard let pagination = pagination else { return }
        if indexPath.row == spots.count - 1 && spots.count < pagination.total && !isLoading{
            getSpots()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cellTapped(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func cellTapped(at index: IndexPath) {
        let vc = DetailSpotVC.instance()
        vc.spot = spots[index.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTapSlideShow(_ sender: UITapGestureRecognizer){
        guard let row = sender.view?.tag else { return }
        let index = IndexPath(row: row, section: 0)
        cellTapped(at: index)
    }
}

//MARK: - Map functions
extension HomeVC {
    
    func animateCameraTo(_ location: CLLocationCoordinate2D) {
        if mapView == nil { return }
        let camera = GMSCameraPosition.camera(withTarget: location, zoom: 16)
        mapView.animate(to: camera)
    }
}

extension HomeVC: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        getMapSpots(mapView)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let spot = marker.userData as? Spot else { return true }
        let vc = DetailSpotVC.instance()
        vc.spot = spot
        self.navigationController?.pushViewController(vc, animated: true)
        return true
    }
}


//MARK: - Location manager
extension HomeVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            if isFirstTime {
                animateCameraTo(location.coordinate)
                isFirstTime = false
            }
            if let coordinates = userLocation {
                if coordinates.latitude != location.coordinate.latitude && coordinates.longitude != location.coordinate.longitude{
                    userLocation = location.coordinate
                }
            } else {
                userLocation = location.coordinate
                clearAndGetSpots()
            }
        }
    }
}

// MARK: - Requests
extension HomeVC {
    
    func deleteSpot(row: Int) {
        guard let id = spots[row].id else { return }
        APIClient().request(request: .DeleteSpot(vc: self, id: id)) { [weak self] (response, success) in
            if success, let `self` = self {
                self.spots.remove(at: row)
            }
        }
    }
    
    func addToFavorites(_ indexPath: IndexPath, btn: UIButton) {
        if !AppDelegate.checkUser(self) { return }
        guard let spotId = self.spots[indexPath.row].id else { return }
        
        APIClient().request(request: .AddToFavorites(vc: self, id: spotId)) { [weak self] (response, success) in
            if success, let `self` = self, let favorite = response.data["favorite"] as? Bool, !favorite {
                self.spots[indexPath.row].favorite = favorite
            }
        }
    }
    
    func getMapSpots(_ mapView: GMSMapView) {
        let ids = mapSpots.map({"\($0.0.id ?? 0)"}).joined(separator: ",")
        let distance = ceil(mapView.getRadius()/1000)
        params.updateValue(distance, forKey: "max_distance")
        params.updateValue(ids, forKey: "spots_ids")
        params.updateValue(mapView.camera.target.latitude, forKey: "lat")
        params.updateValue(mapView.camera.target.longitude, forKey: "lng")
        
        APIClient().request(request: .MapSpots(params: params)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            if success {
                guard let spotsData = response.data["spots"] as? [JSON],
                    let spots = try? spotsData.flatMap(Spot.init) else { return }
                
                self.mapSpots.append(contentsOf: spots.map({($0, nil)}))
            }
        }
    }
    
    func getSpots() {
        var page = 1
        
        if let pagination = pagination {
            page = pagination.currentPage + 1
        }
        
        if var location = userLocation {
            if let locationSent = locationSent {
                location = locationSent
            }
            params.updateValue(location.latitude, forKey: "lat")
            params.updateValue(location.longitude, forKey: "lng")
            //            params.updateValue("50", forKey: "max_distance")
            locationSent = location
        }
        
        if let distance = maxDistance {
            params.updateValue(distance, forKey: "max_distance")
        } else {
            params.removeValue(forKey: "max_distance")
        }
        
        isLoading = true
        APIClient().request(request: .SpotsFilter(params: params, page: page)) { [weak self] (response, success) in
            self?.isLoading = false
            LilithProgressHUD.hide(self?.view)
            guard let `self` = self else { return }
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            if success {
                guard let spotsData = response.data["spots"] as? [JSON]
                    , let paginationData = response.data["pagination"] as? JSON,
                    var spots = try? spotsData.flatMap(Spot.init),
                    let  pagination = try? Pagination(paginationData) else { return }
                
                self.pagination = pagination
                
                if page == 1 {
                    self.spots = []
                }
                
                if let location = self.userLocation {
                    spots = spots.sorted(by: {$0.distance(to: location) < $1.distance(to: location)})
                }
                
                self.spots.append(contentsOf: spots)
            }
        }
    }
}
