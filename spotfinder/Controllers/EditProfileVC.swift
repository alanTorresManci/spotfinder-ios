//
//  EditProfileVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 18/02/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit
import TextFieldEffects

class EditProfileVC: UIViewController {
    @IBOutlet weak var editPassContainer: UIView!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var nameTxt: HoshiTextField!
    @IBOutlet weak var passTxt: HoshiTextField!
    @IBOutlet weak var confPassTxt: HoshiTextField!
    @IBOutlet weak var currPassTxt: HoshiTextField!
    @IBOutlet weak var changePassBtn: UIButton!
    
    var user: User!
    
    var newImage: UIImage?
    
    var isChangingPass = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let user = User.getFromDefaults() else {
            AppDelegate.logout()
            return
        }
        
        self.user = user

        fillData()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Save", comment: ""), style: .plain, target: self, action: #selector(validate))
        
        editPassContainer.isHidden = user.isFromFacebook()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateChangePassView(animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        imageContainer.setCornerRadius(4)
    }
    
    func fillData() {
        profileIV.downloadWithLoader(url: user.image, color: UIColor.lightGray, largeLoader: false)
        nameTxt.text = user.name
    }
    
    func updateChangePassView(animated: Bool) {
        UIView.animate(withDuration: animated ? 0.25 : 0) {
            self.editPassContainer.isHidden = !self.isChangingPass
            self.editPassContainer.alpha = self.isChangingPass ? 1 : 0
            self.changePassBtn.setTitle(self.isChangingPass ? NSLocalizedString("Cancel", comment: "") : NSLocalizedString("Change password", comment: ""), for: .normal)
        }
    }
    
    @IBAction func pickPhoto(_ sender: Any) {
        ImagePicker.pickPhoto(vc: self)
    }
    
    @IBAction func changePass(_ sender: Any) {
        isChangingPass = !isChangingPass
        updateChangePassView(animated: true)
    }
    
    @objc func validate() {
        var message: String?
        var params: JSON = [:]
        if let name = nameTxt.text, !name.isEmpty {
            params.updateValue(name, forKey: "name")
        } else {
            message = NSLocalizedString("Please enter a name", comment: "")
        }
        
        if let image = newImage {
            params.updateValue(image.base64(), forKey: "image")
        }
        
        if isChangingPass {
            if let currentPass = currPassTxt.text, !currentPass.isEmpty {
                if let pass = passTxt.text, !pass.isEmpty {
                    if let confPass = confPassTxt.text, !confPass.isEmpty {
                        if pass == confPass {
                            params.updateValue(currentPass, forKey: "last_password")
                            params.updateValue(pass, forKey: "password")
                            params.updateValue(confPass, forKey: "confirm_password")
                        } else {
                            message = NSLocalizedString("The passwords doesn't match.", comment: "")
                        }
                    } else {
                        message = NSLocalizedString("Please confirm the new password", comment: "")
                    }
                } else {
                    message = NSLocalizedString("Please enter the new password", comment: "")
                }
            } else {
                message = NSLocalizedString("Please enter your current password", comment: "")
            }   
        }
        if let message = message {
            AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: message, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
            return
        }
        
        updateProfile(params: params)
    }
}

// MARK: - IMAGEPICKER
extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            newImage = image.resizeImage(maxSize: 1000)
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            newImage = image.resizeImage(maxSize: 1000)
        } else { print ("error") }
        
        if let image = newImage {
            profileIV.image = image
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("image picker cancel")
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - REQUESTS
extension EditProfileVC {
    func updateProfile(params: JSON) {
        self.view.endEditing(true)
        APIClient().request(request: .UpdateProfile(vc: self, params: params)) { [weak self] (response, success) in
            if success {
                guard let `self` = self, let userDic = response.data["user"] as? JSON,
                    let user = try? User(userDic) else { return }
                user.saveToDefaults()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
