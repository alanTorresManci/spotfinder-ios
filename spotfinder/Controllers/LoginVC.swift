//
//  LoginVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 24/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import Simplicity
import TextFieldEffects
import Firebase

class LoginVC: UIViewController {
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var forgotPassBtn: UIButton!
    @IBOutlet weak var userTxt: HoshiTextField!
    @IBOutlet weak var passTxt: HoshiTextField!
    @IBOutlet weak var btnsContainer: UIView!
    @IBOutlet weak var txtsContainer: UIView!

    var isInLoginByEmail = false
    var isInForgotPass = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if User.getFromDefaults() != nil {
            let vc = MainTabBarController.instance()
            AppDelegate.sharedInstance().switchRootViewController(vc: vc, animated: false)
        }
        styleViews()
        setUpViewsVisibility()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validator() {
        let email = userTxt.text!
        let password = passTxt.text!
        
        if !email.isEmpty &&  !password.isEmpty {
            oauth(email: email, password: password, completion: { (success) in
                if success {
                    self.requestUser()
                } else {
                    AlertsController.showAlert(delegate: self, message: "Usuario o contraseña incorrectos.", OKButtonTitle: "Aceptar")
                }
            })
        } else {
            AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: NSLocalizedString("All fields are required", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""))
        }
    }
    
    func goToHome() {
        let vc = MainTabBarController.instance()
        AppDelegate.sharedInstance().switchRootViewController(vc: vc, animated: true)
    }
    
    func setUpViewsVisibility() {
        self.facebookBtn.visibilityWithAlpha(isHidden: false)
        self.registerBtn.visibilityWithAlpha(isHidden: false)
        self.txtsContainer.visibilityWithAlpha(isHidden: true)
        self.btnsContainer.visibilityWithAlpha(isHidden: true)
    }
    
    func styleViews() {
        style(btn: facebookBtn)
        style(btn: emailBtn)
        style(btn: registerBtn)
        facebookBtn.setTitle(NSLocalizedString("Login by Facebook", comment: ""), for: .normal)
        emailBtn.setTitle(NSLocalizedString("Log in", comment: ""), for: .normal)
        registerBtn.setTitle(NSLocalizedString("Sign up", comment: ""), for: .normal)
    }
    
    func setCornerRadiusFor(btn: UIButton) {
        btn.setCornerRadius(5)
    }
    
    func style(btn: UIButton) {
        btn.titleLabel?.numberOfLines = 1
        btn.titleLabel?.adjustsFontSizeToFitWidth = true
        btn.titleLabel?.lineBreakMode = . byClipping
        setCornerRadiusFor(btn: btn)
        btn.addShadow()
    }
    
    func animateLoginByEmail() {
        self.view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.15,animations:  {
            self.facebookBtn.visibilityWithAlpha(isHidden: !self.isInLoginByEmail)
            self.registerBtn.visibilityWithAlpha(isHidden: !self.isInLoginByEmail)
            self.txtsContainer.visibilityWithAlpha(isHidden: self.isInLoginByEmail)
            self.btnsContainer.visibilityWithAlpha(isHidden: self.isInLoginByEmail)
        }, completion: { (success) in
            self.view.isUserInteractionEnabled = true
            self.isInLoginByEmail = !self.isInLoginByEmail
        })
    }
    
    func animateForgotPass() {
        self.view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.15,animations:  {
            self.passTxt.visibilityWithAlpha(isHidden: !self.isInForgotPass)
            self.emailBtn.setTitle(!self.isInForgotPass ? NSLocalizedString("Send code", comment: "") : NSLocalizedString("Log in", comment: ""), for: .normal)
            self.forgotPassBtn.setTitle(!self.isInForgotPass ? NSLocalizedString("Already hava a code", comment: "") : NSLocalizedString("Forgot my password", comment: ""), for: .normal)
        }, completion: { (success) in
            self.view.isUserInteractionEnabled = true
            self.isInForgotPass = !self.isInForgotPass
            self.isInLoginByEmail = !self.isInForgotPass
        })
        
    }
    
    @IBAction func fbPressed(_ sender: UIButton) {
        Simplicity.login(Facebook()) { (accessToken, error) in
            if let token = accessToken {
                self.oauth(fbToken: token, completion: { (success) in
                    if success {
                        self.requestUser()
                    }
                })
            }
        }
    }
    
    @IBAction func emailPressed(_ sender: UIButton) {
        if isInForgotPass {
            recoverPass()
        } else if !isInLoginByEmail {
            animateLoginByEmail()
        } else {
            validator()
        }
    }
    
    @IBAction func registerPressed(_ sender: UIButton) {
        let vc = RegisterVC.instance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func outOfLoginByEmail(_ sender: UIButton) {
        if isInLoginByEmail {
            animateLoginByEmail()
        } else if isInForgotPass {
            animateForgotPass()
        }
    }
    
    @IBAction func forgotPassword(_ sender: UIButton) {
        if isInForgotPass {
            let vc = UpdatePassVC.instance()
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            animateForgotPass()
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }

}

// MARK: - TextfieldDelegate
extension LoginVC: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userTxt {
            passTxt.becomeFirstResponder()
        } else if textField == passTxt {
            validator()
        }
        return true
    }
}

//MARK: REQUEST
extension LoginVC {
    
    func oauth(email: String, password: String, completion: @escaping (_ success: Bool) -> Void) {
        
        let params: JSON = [
            "grant_type": "password",
            "username": email,
            "password": password,
            "client_id": K.Server.clientId,
            "client_secret": K.Server.clientSecret
        ]
        
        oauth(params: params, completion: completion)
        
    }
    
    func oauth(fbToken: String, completion: @escaping (_ success: Bool) -> Void) {
        
        let params: JSON = [
            "grant_type": "social",
            "network": "facebook",
            "access_token": fbToken,
            "client_id": K.Server.clientId,
            "client_secret": K.Server.clientSecret
        ]
        
        oauth(params: params, completion: completion)
    }
    
    func oauth(params: JSON, completion: @escaping (_ success: Bool) -> Void) {
        
        APIClient().request(request: .LoginByEmail(vc: self, params: params)) { (response ,success) in
            if success {
                UserDefaults.standard.setValue(response.data["access_token"] as? String, forKey: K.Defaults.AccessToken)
                UserDefaults.standard.setValue(response.data["refresh_token"] as? String, forKey: K.Defaults.RefreshToken)
                UserDefaults.standard.setValue(
                    Date().addingTimeInterval(response.data["expires_in"] as! Double).toStringWith(format: K.Defaults.DateFormat),
                    forKey: K.Defaults.TokenExpiresAt)
                completion(true)
            } else {
                completion(false)
            }
        }
        
    }
    
    func requestUser() {
        var params: JSON?
        if let fmcToken = Messaging.messaging().fcmToken, AppDelegate.getDeviceId() == nil {
            params = [
                "fcm_token" : fmcToken,
                "os" : "ios"
            ]
        }
        print("old device_id(getUser): \(AppDelegate.getDeviceId() ?? 0)")
        APIClient().request(request: .GetUser(vc: self, params: params ?? [:])) { [weak self] (response,success) in
            if success {
                guard let `self` = self, let userDic = response.data["user"] as? JSON
                    , let user = try? User(userDic) else { return }
                user.saveToDefaults()
                if let deviceId = response.data["device_id"] as? Int {
                    print("new device_id(getUser): \(deviceId)")
                    AppDelegate.setDeviceId(deviceId: deviceId)
                }
                self.goToHome()
            }
        }
    }
    
    func recoverPass() {
        guard let email = userTxt.text, !email.isEmpty else {
            AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: NSLocalizedString("Please enter an email", comment: ""), OKButtonTitle: "OK")
            return
        }
        
        let params: JSON = [
            "email": email
        ]
        
        APIClient().request(request: .RecoverPassword(vc: self, params: params)) { [weak self] (response,success) in
            guard let `self` = self else { return }
            if success {
                let vc = UpdatePassVC.instance()
                self.navigationController?.pushViewController(vc, animated: true)
                self.animateForgotPass()
            }
        }
    }
    
}
