//
//  SettingsVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 18/02/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func editProfile(_ sender: Any) {
        let vc = EditProfileVC.instance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func about(_ sender: Any) {
        let vc = AboutVC.instance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func logout(_ sender: Any) {
        AppDelegate.logout()
    }

}
