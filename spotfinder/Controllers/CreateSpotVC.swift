//
//  CreateSpotVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation

class CreateSpotVC: UIViewController {
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var titleTxt: UITextField!
    @IBOutlet weak var categoryTxt: UITextField!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var diffTxt: UITextField!
    @IBOutlet weak var mainStack: UIStackView!
    
    var location: Location!
    var diff: Int = 0 {
        didSet{
            diffTxt.text = self.diff > 0 ? "\(self.diff)" : ""
        }
    }
    
    var categories: [Category] = []
    var selectedCategory: Category?
    
    var images: [UIImage] = [UIImage()] {
        didSet {
            reloadCollectionView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Ok", comment: ""), style: .plain, target: self, action: #selector(submit))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .plain, target: self, action: #selector(close))
        for subview in mainStack.subviews {
            subview.layer.cornerRadius = 1
        }
    }
    
    override func viewDidLayoutSubviews() {
        reloadCollectionView()
    }
    
    func selectCategory() {
        let pickerVC = PickerVC.instance()
        pickerVC.list = [categories.map({$0.title})]
        pickerVC.completion = { (section, row) in
            self.selectedCategory = self.categories[row]
            self.categoryTxt.text = self.categories[row].title
        }
        pickerVC.modalPresentationStyle = .overCurrentContext
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    func pickPhoto() {
        ImagePicker.pickPhoto(vc: self)
    }
    
    func getBase64From(_ images: [UIImage]) -> String {
        return images.map({$0.base64()}).joined(separator: " ")
    }
    
    @objc func submit() {
        self.view.endEditing(true)
        
        var message = ""
        
        if titleTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter a title", comment: "")
        } else if descriptionTextView.text.isEmpty {
            message = NSLocalizedString("Please enter a description", comment: "")
        } else if location == nil {
            message = NSLocalizedString("Please select a location", comment: "")
        } else if images.count < 2 {
            message = NSLocalizedString("Please select some images", comment: "")
        } else if selectedCategory == nil {
            message = NSLocalizedString("Please select a category", comment: "")
        } else if diff == 0 {
            message = NSLocalizedString("Please select a difficulty", comment: "")
        } else {
            create()
            return
        }
        
        AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: message, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
    }
    
// MARK: - ACTIONS
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func selectDiff() {
        let pickerVC = PickerVC.instance()
        pickerVC.list = [["1", "2", "3", "4", "5"]]
        var selected = 1
        pickerVC.completion = { (section, row) in
            selected = row + 1
        }
        
        pickerVC.onExit = {
            self.diff = selected
        }
        pickerVC.modalPresentationStyle = .overCurrentContext
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    @IBAction func diff(_ sender: Any) {
        selectDiff()
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        self.view.endEditing(true)
        let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapVC
        mapVC.isSelectingLocation = true
        mapVC.delegate = self
        mapVC.location = location
        self.navigationController?.pushViewController(mapVC, animated: true)
    }
    
    @IBAction func selectCategory(_ sender: Any) {
        guard !categories.isEmpty else { getCategories(); return }
        self.selectCategory()
    }
}

//MARK: - TEXTVIEW
extension CreateSpotVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        let sizeThatFits: CGSize = textView.sizeThatFits(CGSize(width: textView.frame.width, height: CGFloat(MAXFLOAT)))
        if sizeThatFits.height < 100 { return }
        descriptionHeight.constant = sizeThatFits.height
    }
}

// MARK: - COLLECTIONVIEW
extension CreateSpotVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func reloadCollectionView() {
        let width = (collectionView.bounds.width - 10)/3
        collectionViewHeight.constant =  CGFloat(Double(width) * ceil(Double(images.count)/3))
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let imageCell = collectionView.dequeueReusableCell(for: indexPath) as ImageCell
        
        let row = indexPath.row
        
        imageCell.deleteButton.tag = row
        imageCell.deleteButton.addTarget(self, action: #selector(self.deleteImage(_:)), for: .touchUpInside)
        
        imageCell.imageView.image = images[row]
        
        imageCell.selectImageView.isHidden = row != 0
        
        return imageCell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView (_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                         sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.bounds.width - 10)/3
        
        let size = CGSize(width: width, height: width)
        
        return size
    }
    
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 && images.count < 6 {
            pickPhoto()
        } else if images.count >= 5 {
            let alert = UIAlertController(title: nil, message: NSLocalizedString("Max. 5 photos", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
        }
    }
    
    @objc func deleteImage(_ sender: AnyObject) {
        let row: Int = sender.tag
        images.remove(at: row)
    }
}


// MARK: - REQUEST
extension CreateSpotVC {
    
    func getCategories() {
        APIClient().request(request: .Categories(vc: self)) { [weak self] (response, success) in
            if success {
                guard let `self` = self, let categoriesJson = response.data["categories"] as? [JSON], let categories = try? categoriesJson.map(Category.init) else { return }
                self.categories = categories
                self.selectCategory()
            }
        }
    }
    
    func create() {
        var submitedImages = self.images
        submitedImages.remove(at: 0)
        let title = titleTxt.text!
        let desc = descriptionTextView.text!
        let lat = location.coordinates.latitude
        let lng = location.coordinates.longitude
        
        let params: [String: Any] = [
            "title": title,
            "summary": desc,
            "lat": lat,
            "lng": lng,
            "images": getBase64From(submitedImages),
            "category_id": selectedCategory!.id,
            "difficulty": diff
        ]
        
        APIClient().request(request: .CreateSpot(vc: self, params: params)) { [weak self] (response, success) in
            if success {
                guard let `self` = self, let spotJson = response.data["spot"] as? [String:Any] else { return }
                let _ = try? Spot(spotJson)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

// MARK: - IMAGEPICKER
extension CreateSpotVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            images.append(image.resizeImage(maxSize: 1000))
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            images.append(image.resizeImage(maxSize: 1000))
        } else { print ("error") }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("image picker cancel")
        picker.dismiss(animated: true, completion: nil)
    }
}
