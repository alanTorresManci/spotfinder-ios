//
//  UpdatePassVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 18/02/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit
import TextFieldEffects

class UpdatePassVC: UIViewController {
    @IBOutlet weak var emailTxt: HoshiTextField!
    @IBOutlet weak var codeTxt: HoshiTextField!
    @IBOutlet weak var passTxt: HoshiTextField!
    @IBOutlet weak var confPassTxt: HoshiTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(validate))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func validate() {
        self.view.endEditing(true)
        
        var message = ""
        
        if emailTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter your email", comment: "")
        } else if codeTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter your code", comment: "")
        } else if passTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter the new password", comment: "")
        } else if confPassTxt.text!.isEmpty {
            message = NSLocalizedString("Please confirm the new password", comment: "")
        } else if passTxt.text != confPassTxt.text {
            message = NSLocalizedString("The passwords doesn't match.", comment: "")
        } else {
            updatePass()
            return
        }
        
        AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: message, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
    }

}

extension UpdatePassVC {
    
    func updatePass() {
        let params: JSON = [
            "email": emailTxt.text!,
            "new_password": passTxt.text!,
            "recover_code": codeTxt.text!
        ]
        
        APIClient().request(request: .UpdatePassword(vc: self, params: params)) { [weak self] (response,success) in
            if success {
                guard let `self` = self else { return }
                AlertsController.showAlert(delegate: self, title: NSLocalizedString("Success", comment: ""), message: NSLocalizedString("The password has been updated", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""), success: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
}
