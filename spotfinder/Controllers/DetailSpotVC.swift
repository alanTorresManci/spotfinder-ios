//
//  DetailSpotVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 21/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation
import IQKeyboardManagerSwift
import ImageSlideshow

class DetailSpotVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userIV: UIImageView!
    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var spotTitle: UILabel!
    @IBOutlet weak var spotDescription: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topBtn: UIButton!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var sendCommentBtn: UIButton!
    @IBOutlet weak var commentTxt: UITextView!
    @IBOutlet weak var commentTxtHeight: NSLayoutConstraint!
    @IBOutlet weak var commentLoader: UIActivityIndicatorView!
    @IBOutlet weak var numOfCommentsLbl: UILabel!
    @IBOutlet weak var numOfLikesLbl: UILabel!
    
    var spot: Spot!
    
    var refreshControl: UIRefreshControl!
    var pagination: Pagination?
    var isLoading = false
    
    var locationManager = CLLocationManager()
    var userLocation = CLLocationCoordinate2D()
    
    let commentTxtPlaceholder = NSLocalizedString("Leave a comment...", comment: "")
    let commentTxtPlaceholderColor = #colorLiteral(red: 0.7233663201, green: 0.7233663201, blue: 0.7233663201, alpha: 1)
    
    var footer: UIView {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        footerView.backgroundColor = UIColor.clear
        let activityIndicator = UIActivityIndicatorView(frame: footerView.frame)
        activityIndicator.activityIndicatorViewStyle = .gray
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        activityIndicator.tag = 1
        footerView.addSubview(activityIndicator)
        return footerView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let headerView = self.tableView.tableHeaderView {
            self.tableView.setTableHeaderView(headerView: headerView)
            self.tableView.updateHeaderViewFrame()
        }
        setUpLocation()
        initViews()
        loadData()
        getComments()
        tableView.tableFooterView = footer
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //View jumping on push because the tabbar
        if let constraint = self.view.constraints.filter({$0.identifier == "bottomConstraint" }).first {
            self.view.removeConstraint(constraint)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.relayoutTableHeaderView()
    }
    
    func initViews() {
        clear(commentTxt)
        commentTxt.delegate = self
        userIV.layer.cornerRadius = 1
        slideshow.layer.cornerRadius = 1
        containerView.layer.cornerRadius = 1
        self.navigationItem.title = NSLocalizedString("Detail", comment: "")
        tableView.estimatedRowHeight = 100
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(self.refreshSpot), for: .valueChanged)
        tableView.addSubview(refreshControl)
        commentLoader.visibilityWithAlpha(isHidden: true)
        slideshow.contentScaleMode = .scaleAspectFill
        let slideShowTGR = UITapGestureRecognizer(target: self, action: #selector(self.didTapImageSlideShow))
        slideshow.addGestureRecognizer(slideShowTGR)
    }
    
    func setUpLocation() {
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func loadData() {
        spotDescription.text = spot.summary
        spotTitle.text = spot.title
        categoryLbl.text = spot.category?.title
        userName.text = spot.user?.name
        numOfCommentsLbl.text = "\(spot.commentsCount ?? 0)"
        numOfLikesLbl.text = "\(spot.favoriteCount ?? 0)"
        setTopBtnImage()
        
        //set images
        slideshow.activityIndicator = DefaultActivityIndicator(style: .gray, color: nil)
        let sources = spot.images.flatMap { AlamofireSource(urlString: $0) }
        slideshow.setImageInputs(sources)
        
        
        if let image = spot.user?.image {
            userIV.downloadWithLoader(url: image, color: UIColor.lightGray, largeLoader: false)
        }
    }
    
    func setTopBtnImage() {
        topBtn.setImage(User.getFromDefaults()?.id == spot.user?.id ? #imageLiteral(resourceName: "menu-dots") :
            (spot.favorite ? #imageLiteral(resourceName: "heart_filled") : #imageLiteral(resourceName: "heart_unfilled")), for: .normal)
    }
    
    func stopLoading() {
        isLoading = false
        UIView.animate(withDuration: 0.4) {
            (self.tableView.tableFooterView?.viewWithTag(1) as? UIActivityIndicatorView)?.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    func startLoading() {
        isLoading = true
        (self.tableView.tableFooterView?.viewWithTag(1) as? UIActivityIndicatorView)?.startAnimating()
    }
    
    func validate(comment: String?) -> Bool {
        guard let comment = comment else { return false }
        return !comment.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    @IBAction func topButtonPressed(_ sender: Any) {
        if User.getFromDefaults()?.id == spot.user?.id {
            showOptionsMenu()
        } else {
            if !AppDelegate.checkUser(self) { return }
            spot.favorite = !spot.favorite
            setTopBtnImage()
            topBtn.isUserInteractionEnabled = false
            addToFavorites()
        }
    }
    
    func showOptionsMenu(){
        AlertsController.showActionSheet(delegate: self, buttons: NSLocalizedString("Delete", comment: ""), handler: { (str) in
            switch str {
            case NSLocalizedString("Delete", comment: ""):
                AlertsController.showAlert(delegate: self, title: NSLocalizedString("Delete spot", comment: ""), message: NSLocalizedString("Are you sure that you want to delete this spot?", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""), otherButtonTitle: NSLocalizedString("Cancel", comment: ""), success: {
                    self.deleteSpot()
                }, cancel: nil)
                break
            default:
                break
            }
        })
    }
    
    @objc func didTapImageSlideShow() {
        slideshow.presentFullScreenController(from: self)
    }
    
    @IBAction func distanceBtnPressed(_ sender: Any) {
        if let coordinates = spot.coordinates() {
            AppDelegate.openMap(self, with: coordinates)
        }
    }
    
    @IBAction func sendComment(_ sender: Any) {
        let comment = commentTxt.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if validate(comment: comment) {
            send(comment: comment)
        }
    }
}

// MARK: - TableView
extension DetailSpotVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spot.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        cell.setUp(for: spot.comments[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let pagination = pagination else { return }
        if indexPath.row == spot.comments.count - 1 && spot.comments.count < pagination.total && !isLoading{
            getComments()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

// MARK: - Textfield
extension DetailSpotVC: UITextViewDelegate {
    
    func adjustTextViewSize() {
        let contentSize = commentTxt.contentSize
        if contentSize.height >= 33 && contentSize.height <= 75 {
            commentTxtHeight.constant = contentSize.height
        }
    }
    
    func textViewDidChange(_ textView: UITextView){
        adjustTextViewSize()
        sendCommentBtn.isEnabled = validate(comment: textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.count > 500 && range.length == 0 {
            return false
        }
    
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == commentTxtPlaceholderColor{
            textView.text = nil
            textView.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty { clear(textView) }
    }
    
    func clear(_ textView: UITextView) {
        textView.text = commentTxtPlaceholder
        textView.textColor = commentTxtPlaceholderColor
        sendCommentBtn.isEnabled = false
        UIView.animate(withDuration: 0.3) {
            self.adjustTextViewSize()
        }
    }
}

//MARK: - Location manager
extension DetailSpotVC: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            if userLocation.latitude != location.coordinate.latitude && userLocation.longitude != location.coordinate.longitude{
                userLocation = location.coordinate
                distanceLbl.text = spot.coordinates()?.readableDistance(to: userLocation)
            }
        }
    }
}

// MARK: - Requests
extension DetailSpotVC {
    
    func refreshSpot() {
        guard let id = spot.id else { return }
        APIClient().request(request: .Spot(vc: nil, id: id)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            if success, let spotJSON = response.data["spot"] as? JSON, let spot = try? Spot(spotJSON) {
                self.spot = spot
                self.pagination = nil
                self.loadData()
                self.tableView.reloadData()
            }
        }
    }
    
    func deleteSpot() {
        guard let id = spot.id else { return }
        APIClient().request(request: .DeleteSpot(vc: self, id: id)) { [weak self] (response, success) in
            if success, let `self` = self {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func addToFavorites() {
        if !AppDelegate.checkUser(self) { return }
        guard let spotId = self.spot.id else { return }
        
        APIClient().request(request: .AddToFavorites(vc: self, id: spotId)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            var favorite = !self.spot.favorite
            if success {
                if let fav = response.data["favorite"] as? Bool {
                    favorite = fav
                }
            }
            self.topBtn.isUserInteractionEnabled = true
            self.spot.favorite = favorite
            self.setTopBtnImage()
        }
    }
    
    func getComments() {
        guard let spotId = self.spot.id else { return }
        var page = 1
        
        if let pagination = pagination {
            page = pagination.currentPage + 1
        }
        
        self.startLoading()
        APIClient().request(request: .SpotComments(id: spotId, page: page)) { [weak self] (response, success) in
            self?.stopLoading()
            if success {
                guard let `self` = self, let commentsData = response.data["comments"] as? [JSON],
                        let comments = try? commentsData.flatMap(Comment.init).sorted(by: {$0.id < $1.id}),
                        let paginationData = response.data["pagination"] as? JSON,
                        let  pagination = try? Pagination(paginationData) else { return }
                
                self.pagination = pagination
                
                if page == 1 {
                    self.spot.comments = []
                }
                
                self.spot.comments.append(contentsOf: comments)
                self.numOfCommentsLbl.text = "\(self.spot.comments.count)"
                self.tableView.reloadData()
            }
        }
    }
    
    func animateCommentLoaderBtn(showLoader: Bool = false) {
        if showLoader {
            commentTxt.endEditing(true)
            self.commentLoader.startAnimating()
        } else {
            self.commentLoader.stopAnimating()
        }
        
        UIView.animate(withDuration: 0.4) {
            self.sendCommentBtn.visibilityWithAlpha(isHidden: showLoader)
            self.commentLoader.visibilityWithAlpha(isHidden: !showLoader)
        }
    }
    
    func send(comment: String) {
        if !AppDelegate.checkUser(self) { return }
        guard let spotId = self.spot.id else { return }
        self.animateCommentLoaderBtn(showLoader: true)
        
        let params: JSON = [
            "comment": comment
        ]
        
        APIClient().request(request: .AddComment(vc: self, id: spotId, params: params)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            self.animateCommentLoaderBtn()
            if success {
                self.clear(self.commentTxt)
                self.spot.commentsCount = self.spot.commentsCount ?? 0 + 1
                self.numOfCommentsLbl.text = "\(self.spot.commentsCount ?? 0)"
                
                guard let commentJSON = response.data["comment"] as? JSON, let comment = try? Comment(commentJSON) else { return }
                if let pagination = self.pagination {
                    if pagination.currentPage == pagination.lastPage {
                        self.spot.comments.append(comment)
                    }
                } else {
                    self.spot.comments.append(comment)
                }
                self.tableView.reloadData()
            }
        }
    }
}
