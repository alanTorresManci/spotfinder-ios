//
//  Comment.swift
//  spotfinder
//
//  Created by Moy Hdez on 29/08/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import Foundation

struct Comment: Codable {
    var id: Int
    var comment: String
    var date: String
    var user: User
    
    enum CodingKeys: String, CodingKey {
        case id
        case comment
        case date = "created_at"
        case user
    }
}
