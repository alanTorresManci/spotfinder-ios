//
//  Category.swift
//  spotfinder
//
//  Created by Moy Hdez on 17/02/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import Foundation

struct Category: Codable {
    var id: Int
    var image: URL
    var title: String
    var summary: String
}

