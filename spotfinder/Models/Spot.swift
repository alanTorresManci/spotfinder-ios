//
//  Spot.swift
//  spotfinder
//
//  Created by Moy Hdez on 11/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation

struct Spot: Codable {
    
    var id: Int?
    var title: String?
    var summary: String?
    var lat: Double?
    var lng: Double?
    var images: [String] = []
    var user: User?
    var category: Category?
    var favorite: Bool = false
    var comments: [Comment] = []
    var favoriteCount: Int?
    var commentsCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case summary
        case lat
        case lng
        case images
        case user
        case category
        case favorite
        case comments
        case favoriteCount = "favorite_count"
        case commentsCount = "comments_count"
    }
    
    func coordinates() -> CLLocationCoordinate2D? {
        guard let lat = self.lat, let lng = self.lng else { return nil }
        let coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        return coordinates
    }
    
    func distance(to location: CLLocationCoordinate2D) -> Double {
        guard let lat = self.lat, let lng = self.lng else { return 10000 }
        let from = CLLocation(latitude: lat, longitude: lng)
        let to = CLLocation(latitude: location.latitude, longitude: location.longitude)
        return from.distance(from: to)
    }
}
