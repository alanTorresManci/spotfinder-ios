//
//  Pagination.swift
//  VinosBajaCalifornia
//
//  Created by Moy Hdez on 24/11/17.
//  Copyright © 2017 tejuino. All rights reserved.
//

import Foundation

public struct Pagination: Codable {
    
    // MARK: Properties
    public var total: Int
    public var perPage: Int
    public var currentPage: Int
    public var lastPage: Int
    
    enum CodingKeys: String, CodingKey {
        case total
        case perPage = "per_page"
        case currentPage = "current_page"
        case lastPage = "last_page"
    }
}
