//
//  Helper.swift
//  spotfinder
//
//  Created by Moy Hdez on 25/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import Foundation
import AlamofireImage
import Alamofire

class Helper {
    func formatNumber(number: Int, isCurrency: Bool = false) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = isCurrency ? .currency : .decimal
        formatter.maximumFractionDigits = 0
        formatter.locale = Locale(identifier: "es_MX")
        
        return formatter.string(from: NSNumber(value: number))!
    }
    
    func formatDate(stringDate: String, fromFormat: String, toFormat: String) -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "es_MX")
        formatter.dateFormat = fromFormat
        let date = formatter.date(from: stringDate)
        formatter.dateFormat = toFormat
        
        if let formatDate = date {
            return formatter.string(from: formatDate)
        }
        
        print("No fue posible dar formato a tu fecha")
        return stringDate
    }
    
    static func getDate(stringDate: String, fromFormat: String) -> Date{
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "es_MX")
        formatter.dateFormat = fromFormat
        return formatter.date(from: stringDate)!
    }
    
    func formatInput(textField: UITextField, maxLength: Int) {
        guard let text = textField.text else {
            return
        }
        
        // remove commas from text
        let numberWithOutCommas = text.replacingOccurrences(of: ",", with: "")
        
        // formatter
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        
        // get number
        var number = formatter.number(from: numberWithOutCommas)
        
        if number != nil {
            
            if numberWithOutCommas.count > maxLength {
                let startIndex = numberWithOutCommas.index(numberWithOutCommas.startIndex, offsetBy: maxLength)
                let formattedStr = numberWithOutCommas.substring(to: startIndex)
                number = formatter.number(from: formattedStr)
            }
            
            textField.text = formatter.string(from: number!)!
        }else {
            textField.text = nil
        }
    }
    
    func downloadImage(urlStr: String, completion: @escaping (_ image: UIImage?, _ success: Bool) -> Void) {
        Alamofire.request(urlStr).responseImage { response in
            if response.result.isSuccess {
                if let image = response.result.value {
                    completion(image, true)
                }
            } else {
                completion(nil, false)
            }
        }
    }
}

extension Date {
    func toStringWith(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_MX") as Locale!
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}
