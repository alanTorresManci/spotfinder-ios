//
//  UIViewController+Ext.swift
//  spotfinder
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

extension UIViewController: StoryboardIdentifiable { }

extension UIViewController {
    class func instance(storyboardName: UIStoryboard.Storyboard = .main) -> Self {
        let storyboard = UIStoryboard(name: storyboardName.rawValue.capitalized, bundle: nil)
        return storyboard.instantiateViewController()
    }
}

