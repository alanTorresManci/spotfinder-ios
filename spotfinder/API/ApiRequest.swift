//
//  ApiManager.swift
//  SafoDriver
//
//  Created by Moy Hdez on 18/10/17.
//  Copyright © 2017 Tejuino. All rights reserved.
//

import Foundation
import Alamofire

enum ApiRequest {
    case GetUser(vc: UIViewController?, params: JSON)
    case Register(vc: UIViewController?, params: JSON)
    case UpdateProfile(vc: UIViewController?, params: JSON)
    case LoginByEmail(vc: UIViewController?, params: JSON)
    case RecoverPassword(vc: UIViewController?, params: JSON)
    case UpdatePassword(vc: UIViewController?, params: JSON)
    case UpdateFCMToken(params: JSON)
    
    case UserSpots(params: JSON, page: Int)
    case UserFavorites(params: JSON, page: Int)
    case SpotsFilter(params: JSON, page: Int)
    case MapSpots(params: JSON)
    case SpotComments(id: Int, page: Int)
    case Spot(vc: UIViewController?, id: Int)
    case DeleteSpot(vc: UIViewController?, id: Int)
    
    case CreateSpot(vc: UIViewController?, params: JSON)
    case AddToFavorites(vc: UIViewController?, id: Int)
    case AddComment(vc: UIViewController?, id: Int, params: JSON)
    
    case Categories(vc: UIViewController?)
    
    var method : Alamofire.HTTPMethod {
        switch self {
        case .GetUser:
            return .get
        case .UserSpots:
            return .get
        case .MapSpots:
            return .get
        case .Spot:
            return .get
        case .SpotsFilter:
            return .get
        case .UserFavorites:
            return .get
        case .SpotComments:
            return .get
        case .Categories:
            return .get
        case .DeleteSpot:
            return .delete
        default:
            return .post
        }
    }

    var url: String {
        switch self {
        case .Register:
            return "api/account/register"
        case .GetUser:
            return "api/account/profile"
        case .UpdateProfile:
            return "api/account/update"
        case .LoginByEmail:
            return "oauth/token"
        case .UserSpots(_, let page):
            return "api/account/spots?page=\(page)"
        case .UserFavorites(_, let page):
            return "api/account/favorites?page=\(page)"
        case .SpotComments(let id, let page):
            return "api/spots/\(id)/comments?page=\(page)"
        case .Spot(_, let id):
            return "api/spots/\(id)"
        case .CreateSpot:
            return "api/spots"
        case .AddToFavorites(_, let id):
            return "api/spots/\(id)/favorites"
        case .DeleteSpot(_, let id):
            return "api/spots/\(id)"
        case .AddComment(_, let id, _):
            return "api/spots/\(id)/comments"
        case .RecoverPassword:
            return "api/account/recover"
        case .UpdatePassword:
            return "api/account/updatePassword"
        case .UpdateFCMToken:
            return "account/updateFCMToken"
        case .MapSpots:
            return "api/spots/map"
        case .SpotsFilter(_, let page):
            return "api/spots/filter?page=\(page)"
        case .Categories:
            return "api/categories"
        }
    }

    var headers: APIClient.Header {
        switch self {
        case .LoginByEmail:
            return .AccessToken
        default:
            return .Default
        }
    }

    var vc: UIViewController? {
        switch self {
        case .Register(let vc, _):
            return vc
        case .GetUser(let vc, _):
            return vc
        case .UpdateProfile(let vc, _):
            return vc
        case .LoginByEmail(let vc, _):
            return vc
        case .Spot(let vc, _):
            return vc
        case .CreateSpot(let vc, _):
            return vc
        case .DeleteSpot(let vc, _):
            return vc
        case .AddToFavorites(let vc, _):
            return vc
        case .AddComment(let vc, _, _):
            return vc
        case .RecoverPassword(let vc, _):
            return vc
        case .UpdatePassword(let vc, _):
            return vc
        case .Categories(let vc):
            return vc
        default:
            return nil
        }
    }

    var params: [String: Any]? {
        switch self {
        case .UpdateProfile(_, let params):
            return params
        case .LoginByEmail(_, let params):
            return params
        case .GetUser(_, let params):
            return params
        case .Register(_, let params):
            return params
        case .CreateSpot(_, let params):
            return params
        case .AddComment(_, _, let params):
            return params
        case .UpdatePassword(_, let params):
            return params
        case .UpdateFCMToken(let params):
            return params
        case .SpotsFilter(let params, _):
            return params
        case .UserSpots(var params, let page):
            params.updateValue("page", forKey: "\(page)")
            return params
        case .UserFavorites(var params, let page):
            params.updateValue("page", forKey: "\(page)")
            return params
        case .MapSpots(let params):
            return params
        default:
            return nil
        }
    }

    var showErrorAlert: Bool {
        switch self {
        default:
            return true
        }
    }
}
