//
//  ApiResponse.swift
//  SafoDriver
//
//  Created by Moy Hdez on 18/10/17.
//  Copyright © 2017 Tejuino. All rights reserved.
//

import Foundation

struct ApiResponse {
    var data: JSON = [:]
    var error: ApiError?
    var code: Int?
    
    init(json: JSON) {
        code = json["code"] as? Int
        if let dataJson = json["data"] as? JSON {
            data = dataJson
        } else {
            data = json
        }
        
        if let errorJson = json["error"] as? JSON {
            error = ApiError(json: errorJson)
        }
    }
    
    init(error: ApiError) {
        self.error = error
    }
}

struct ApiError {
    var user: String?
    var debug: String?
    
    init(json: JSON) {
        user = json["user"] as? String
        debug = json["debug"] as? String
    }
}
