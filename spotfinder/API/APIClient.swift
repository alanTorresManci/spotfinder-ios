//
//  APIClient.swift
//  designado
//
//  Created by Moy Hdez on 25/04/17.
//  Copyright © 2017 CJAPPS. All rights reserved.
//

import UIKit
import Alamofire

typealias JSON = [String:Any]

class APIClient {
    
    enum Header: Int {
        case Default = 0
        case AccessToken = 1
        case Empty = 2
        
        var header: HTTPHeaders? {
            switch self {
            case .Default:
                guard let accessToken = UserDefaults.standard.value(forKey: K.Defaults.AccessToken) else { return ["Accept":"application/json","locale": Locale.current.languageCode ?? "es"] }
                return ["Accept":"application/json","Authorization":"Bearer \(accessToken)", "locale": Locale.current.languageCode ?? "es"]
            case .AccessToken:
                return ["Content-Type":"application/x-www-form-urlencoded", "locale": Locale.current.languageCode ?? "en"]
            case .Empty:
                return  ["Accept":"application/json","locale": Locale.current.languageCode ?? "en"]
            }
        }
    }
    
    func request(request: ApiRequest, completion: @escaping (_ response: ApiResponse, _ success: Bool) -> Void) {
        if let view = request.vc?.view {
            LilithProgressHUD.show(view)
        }
        
        Alamofire.request(K.Server.BaseURL+request.url, method: request.method, parameters: request.params, encoding: URLEncoding.default, headers: request.headers.header).responseJSON { (response) -> Void in
                
                if let view = request.vc?.view {
                    LilithProgressHUD.hide(view)
                }
                
                var apiResponse = ApiResponse(error: ApiError(json: [
                    "user": "Estamos experimentando problemas internos, por favor intente mas tarde",
                    "debug": "Server response is not valid"
                    ]))
                
                if let json = response.value as? JSON {
                    apiResponse = ApiResponse(json: json)
                }
                
                completion(apiResponse, (200...300).contains(response.response?.statusCode ?? 500))
                if request.showErrorAlert {
                    self.handle(apiResponse, for: request.vc)
                }
        }
    }
    
    func handle(_ response: ApiResponse, for vc: UIViewController? ) {
        if let error = response.error, let vc = vc {
            AlertsController.showAlert(delegate: vc, title: nil, message: error.user, OKButtonTitle: "OK")
        }
    }
}

