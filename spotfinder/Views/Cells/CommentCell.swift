//
//  CommentCell.swift
//  spotfinder
//
//  Created by Moy Hdez on 29/08/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userNameBtn: UIButton!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var imgBtn: UIButton!
    
    func setUp(for comment: Comment) {
        dateLbl.text = comment.date
        commentLbl.text = comment.comment
        userNameBtn.setTitle(comment.user.name, for: .normal)
        
        userImg.downloadWithLoader(url: comment.user.image, color: UIColor.lightGray, largeLoader: false)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
