//
//  SpotCell
//  spotfinder
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation
import ImageSlideshow

class SpotCell: UITableViewCell {
    
    @IBOutlet weak var userIV: UIImageView!
    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var spotTitle: UILabel!
    @IBOutlet weak var spotDescription: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var topBtn: UIButton!
    @IBOutlet weak var distanceBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        userIV.layer.cornerRadius = 1
        slideshow.layer.cornerRadius = 1
        containerView.layer.cornerRadius = 1
        slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
        slideshow.contentScaleMode = .scaleAspectFill
    }
    
    
    func setUpWith(spot: Spot, location: CLLocationCoordinate2D?){
        spotDescription.text = spot.summary
        spotTitle.text = spot.title
        userName.text = spot.user?.name
        if let location = location {
            distanceLbl.text = spot.coordinates()?.readableDistance(to: location)
        }
        
        topBtn.setImage(User.getFromDefaults()?.id == spot.user?.id ? #imageLiteral(resourceName: "menu-dots") :
            (spot.favorite ? #imageLiteral(resourceName: "heart_filled") : #imageLiteral(resourceName: "heart_unfilled")), for: .normal)
        
        let sources = spot.images.flatMap { AlamofireSource(urlString: $0) }
        slideshow.setImageInputs(sources)
        
        if let image = spot.user?.image {
            userIV.downloadWithLoader(url: image, color: UIColor.lightGray, largeLoader: false)
        }
    }

}
